Guruprasad's dotfiles
=====================

This repository contains my dotfiles to be used with [chezmoi](https://github.com/twpayne/chezmoi).


Dependencies
------------
* git.
* chezmoi.
* Bitwarden CLI for retrieving secrets from Bitwarden.
* Any other dependencies needed by the configuration files in this repository.

License
-------
This repository is licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) or later.
